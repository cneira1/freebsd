# $FreeBSD: releng/11.2/stand/efi/Makefile.inc 329146 2018-02-12 01:17:06Z kevans $

# Options used when building app-specific efi components
# See conf/kern.mk for the correct set of these
CFLAGS+=	-Wformat
LDFLAGS+=	-nostdlib

.if ${MACHINE_CPUARCH} == "amd64"
CFLAGS+=	-fshort-wchar
CFLAGS+=	-mno-red-zone
.endif

.if ${MACHINE_CPUARCH} == "aarch64"
CFLAGS+=	-fshort-wchar
CFLAGS+=	-fPIC
.endif

.if ${MACHINE_CPUARCH} == "arm"
CFLAGS+=	-fPIC
.endif

.include "../Makefile.inc"
