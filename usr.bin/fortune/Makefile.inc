# $FreeBSD: releng/11.2/usr.bin/fortune/Makefile.inc 321266 2017-07-20 00:50:01Z ngie $

FORTUNE_SRC=	${SRCTOP}/usr.bin/fortune
FORTUNE_OBJ=	${OBJTOP}/usr.bin/fortune

.include "${SRCTOP}/usr.bin/Makefile.inc"
