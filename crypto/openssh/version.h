/* $OpenBSD: version.h,v 1.79 2017/03/20 01:18:59 djm Exp $ */
/* $FreeBSD: releng/11.2/crypto/openssh/version.h 323136 2017-09-02 23:39:51Z des $ */

#define SSH_VERSION	"OpenSSH_7.5"

#define SSH_PORTABLE	"p1"
#define SSH_RELEASE	SSH_VERSION SSH_PORTABLE

#define SSH_VERSION_FREEBSD	"FreeBSD-20170903"

#ifdef WITH_OPENSSL
#define OPENSSL_VERSION	SSLeay_version(SSLEAY_VERSION)
#else
#define OPENSSL_VERSION	"without OpenSSL"
#endif
