/* $FreeBSD: releng/11.2/lib/libelftc/elftc_version.c 320685 2017-07-05 16:39:29Z emaste $ */

#include <sys/types.h>
#include <libelftc.h>

const char *
elftc_version(void)
{
	return "elftoolchain r3561M";
}
