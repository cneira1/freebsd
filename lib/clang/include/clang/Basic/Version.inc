/* $FreeBSD: releng/11.2/lib/clang/include/clang/Basic/Version.inc 331838 2018-03-31 11:38:16Z dim $ */

#define	CLANG_VERSION			6.0.0
#define	CLANG_VERSION_STRING		"6.0.0"
#define	CLANG_VERSION_MAJOR		6
#define	CLANG_VERSION_MINOR		0
#define	CLANG_VERSION_PATCHLEVEL	0

#define	CLANG_VENDOR			"FreeBSD "

#define	SVN_REVISION			"326565"
