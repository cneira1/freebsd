// $FreeBSD: releng/11.2/lib/clang/include/lld/Common/Version.inc 332899 2018-04-24 00:47:17Z emaste $

#define LLD_VERSION 6.0.0
#define LLD_VERSION_STRING "6.0.0"
#define LLD_VERSION_MAJOR 6
#define LLD_VERSION_MINOR 0

#define LLD_REPOSITORY_STRING "FreeBSD"
// <Upstream revision at import>-<Local identifier in __FreeBSD_version style>
#define LLD_REVISION_STRING "326565-1100001"
