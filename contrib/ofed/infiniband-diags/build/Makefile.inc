# $FreeBSD: releng/11.2/contrib/ofed/infiniband-diags/build/Makefile.inc 329564 2018-02-19 12:21:24Z hselasky $

.PATH: ${.CURDIR}/../../src ${.CURDIR}/../../man

BINDIR?= /usr/bin
SRCS+= ibdiag_common.c ibdiag_sa.c
CFLAGS+= -I${SYSROOT:U${DESTDIR}}/${INCLUDEDIR}/infiniband
CFLAGS+= -DHAVE_CONFIG_H=1
CFLAGS+= -I${.CURDIR}/../../
CFLAGS+= -I${.CURDIR}/../../src
LIBADD+= osmcomp ibmad ibumad

